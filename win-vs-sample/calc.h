#pragma once
class Calc {
private:
	int a;
	int b;

public:
	Calc() {}
	~Calc() {}
	int calc_plus(int n, int m);
	int calc_minus(int n, int m);
};